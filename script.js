const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

// const sonoplastia = {
//     passo: function(){
//         const step = new Audio(step.wav);
//         step.play();
//     },
// };

const Player = {
    ArrowUp: function(){
        return 'y-';
    },
    ArrowDown:function(){
        return 'y+';
    },
    ArrowLeft:function(){
        return 'x-';
    },
    ArrowRight:function(){
        return 'x+';
    },
    playerStep:function(){
        const step = new Audio('./step.wav');
        step.play();
    },
    playerpositionx: 0,
    playerpositiony:9,
    placePlayer: function(x,y,ArrayMap){
        if(y=='y+'&&ArrayMap[this.playerpositiony+1][this.playerpositionx]!='W'&&this.playerpositiony+1<=14){
            this.playerpositiony++; 
           this.playerStep()        
        } 
        if(y=='y-'&&ArrayMap[this.playerpositiony-1][this.playerpositionx]!='W'&&this.playerpositiony-1>=0){
            this.playerpositiony--; 
            this.playerStep()           
        }
        if(x=='x+'&&ArrayMap[this.playerpositiony][this.playerpositionx+1]!='W'&&this.playerpositionx+1<=20){
            this.playerpositionx++;  
            this.playerStep()           
        } 
        if(x=='x-'&&ArrayMap[this.playerpositiony][this.playerpositionx-1]!='W'&&this.playerpositionx-1>=0){
            this.playerpositionx--; 
            this.playerStep()             
        }       
    },
    playerwins: function(){
        if(document.getElementById('#8#20').classList.contains("player")){
            console.log("winner");
            const winlocal = document.getElementById("winner_area");
            winlocal.removeChild(document.getElementById("winner_area").firstChild);
            const textwin = document.createTextNode("Você Encontrou a Saída!!!");
            winlocal.appendChild(textwin);
            const winsong = new Audio("./winsong.wav");
            winsong.play();

        }
    },   
};

const Labirinto = {
    render:function(ArrayMap,tamanho){
        let blocotam = tamanho/21;
    for(let i in ArrayMap){
        const linha = document.createElement('div');
            linha.className = "linha";
            linha.id = i;
            const destino = document.getElementById("gameSection");
            destino.appendChild(linha);
        for(let j=0;j<ArrayMap[i].length;j++){
            
            if(ArrayMap[i][j]=='W'){
                const parede = document.createElement('div');
                parede.className = "blocoCheio";
                if(j==Player["playerpositionx"]&&i==Player["playerpositiony"]){
                    parede.className='player'; 
                }
                parede.id = '#'+i+'#'+j;
                parede.style.width = blocotam+'px';
                parede.style.height=blocotam+'px';
                linha.appendChild(parede);
            }
            
            else if(ArrayMap[i][j]==' '){
                const parede = document.createElement('div');
                parede.className = "blocoVazio";
                if(j==Player["playerpositionx"]&&i==Player["playerpositiony"]){
                    parede.className='player'; 
                }
                parede.id = '#'+i+'#'+j;
                parede.style.width = blocotam+'px';
                parede.style.height=blocotam+'px';
                linha.appendChild(parede);
            }
            else if(ArrayMap[i][j]=='S'){
                const parede = document.createElement('div');
                parede.id ='#'+i+'#'+j;
                parede.className = 'blocoStart'
                if(j==Player["playerpositionx"]&&i==Player["playerpositiony"]){
                    parede.className='player'; 
                }
                parede.style.width = blocotam+'px';
                parede.style.height=blocotam+'px';
                linha.appendChild(parede);
            }
            else if(ArrayMap[i][j]=='F'){
                const parede = document.createElement('div');
                parede.id = '#'+i+'#'+j;
                parede.className = 'blocoFinal'
                if(j==Player["playerpositionx"]&&i==Player["playerpositiony"]){
                    parede.className='player'; 
                }
                parede.style.width = blocotam+'px';
                parede.style.height=blocotam+'px';
                linha.appendChild(parede);
            }
            
            
            
        }
    }

    } 
};
Labirinto.render(map,1000);

addEventListener('keydown',function(event){
    const keyname = event.key;
    const moves = Player[keyname];
    Player.placePlayer(moves(),moves(),map);
    document.getElementById("gameSection").innerHTML = "";
    Labirinto.render(map,1000);
    Player.playerwins();
});
